#include <stdio.h>

void print(int num)
{
    if (num == 0)
        return;
    printf("%d ", num);
    print(num - 1);
}

void patter(int n, int i)
{
    if (n == 0)
        return;
    print(i);
    printf("\n");
    patter(n - 1, i + 1);
}

int main()
{
    int n;
    printf("Enter the pattern > ");
    scanf("%d", &n);
    patter(n, 1);
    return 0;
}

