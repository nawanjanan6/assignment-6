#include <stdio.h>

int fibonacci(int y)
{
    if(y==0)
        return 0;
    else if(y==1)
        return 1;
    else
        return (fibonacci(y-1)+fibonacci(y-2));
}

int count=0, x=0;

int fibonacciSeq(int n)
{
    if(count<=n)
    {
        printf("%d \n", fibonacci(x));
        x++;
        count++;
        fibonacciSeq(n);
    }
    else
        return 0;
}

int main()
{
    int n;
    printf("Please input the Limit : ");
    scanf("%d", &n);

    fibonacciSeq(n);

    return 0;
}
